#!/bin/bash
#
# Author: Helmut Flasche <flasche@ient.rwth-aachen.de>, Christian Rohlfing <rohlfing@ient.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
infolder="./pdfs/sorted"
outfolder="./pdfs/watermarked"
tmpfolder="/tmp"
cores=$(nproc)
dpi="250"
quality="25"
v=n
author="IENT RWTH Aachen, 2020"
split=n

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--out outfolder] [--cores numcores] [--quality jpegquality] --  watermark exam scans with matriculation number in folder 'in' and puts them in folder 'out'. 
Attention: contents of folder 'out' will be overwritten in the following!

Options:
  -h, --help      show this help text
  -i, --in        input folder with PDFs. Default: ${infolder}
  -o, --out       output folder. Default: ${outfolder}
  -t, --tmp       parent of temporary folder. Default ${tmpfolder}
  -j, --cores     number of cores for parallel processing. Default: ${cores}
  -d, --dpi       dpi parameter for conversion from pdf to images. Default: ${dpi}
  -q, --quality   quality parameter for jpeg. Default: ${quality}
  -a, --author    author of the PDF file. Default: ${author}
      --splita3   split A3 landscape pages (brochure-scan) to single A4 portrait pages and resort them
  -v, --verbose   "


# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi


OPTIONS=hi:o:j:d:q:vt:a:
LONGOPTS=help,in:,out:,cores:,dpi:,quality:,splita3,verbose,tmp:,author:


# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # e.g. return value is 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"


# now enjoy the options in order and nicely split until we see --
while true; do
  case "$1" in
    -h|--help)
      echo "$usage"
      exit
      ;;
    -i|--in)
      infolder="$2"
      shift 2
      ;;
    -t|--tmp)
       tmpfolder="$2"
       shift 2
       ;;
    -o|--out)
      outfolder="$2"
      shift 2
      ;;
    -j|--cores)
      cores="$2"
      shift 2
      ;;
    -d|--dpi)
      dpi="$2"
      shift 2
      ;;
    -q|--quality)
      quality="$2"
      shift 2
      ;;
    --splita3)
      split=y
      shift 1
      ;;
    -v|--verbose)
      v=y
      shift
      ;;
    -a|--author)
      author="$2"
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Programming error"
      exit 3
      ;;
  esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder" \
        "$tmpfolder"
do
  if ! [ -d "$f" ]; then
    echo "Folder $f does not exist. Exiting."
    exit
  fi
done
#}}}


# List all PDFs
echo
echo "Available PDFs"
echo
ls -a1 "${infolder}"/*.pdf
echo

# Loop over all PDFs
#for longinpdf in "${infolder}"/*.pdf
#do
export SHELL=$(type -p bash)
doit () {
  longinpdf=$1
  outfolder=$2
  dpi=$3
  quality=$4
  split=$5
  lauthor=$6
  pretmpfolder=$7

  # Create temporary folder per job
  tmpfolder="$(mktemp -d -p ${pretmpfolder})"

  # Get matriculation number from file
  inpdf=$(basename "${longinpdf%.*}")  # file name without folder and extension
  matnum=${inpdf:0:6}  # read in first 6 letters

  case $matnum in
    ''|*[!0-9]*) echo "Warning: ${matnum} not a matriculation number. I'm ignoring it for now." && exit ;;
  esac

  echo
  echo "Handle ${longinpdf}"

  # if already watermarked
  if [[ $matnum = "waterm" ]];
  then
    echo
    echo "Warning: ${longinpdf} is already watermarked"
    exit
  fi

  # Convert PDF into images
  # This may take a while depending on ${dpi} 
  # (which should not be set to a higher value than the DPI of the scanning device!)
  echo
  echo "Convert PDF into images"
  pdftoppm -r ${dpi} -png $longinpdf "${tmpfolder}"/page

  # Split A3 pages
  # scanning exams with the feeder of the copy machine results in double paged A3
  # here, we split these A3 pages into two A4 ones and resort them accordingly
  # which is the inverse of "brochure print"
  if [[ "$split" = "y" ]]; then
    echo
    echo "Split landscape A3 pages into each two portrait A4 pages"
    
    # rename A3 pages
    for longpage in "${tmpfolder}"/page*.png; do
      page=$(basename "${longpage}")
      mv "$longpage" "${tmpfolder}/tmppage_a3${page}"
    done

    pcnt=1
    for longa3page in "${tmpfolder}"/tmppage_a3*.png; do
      echo "${longa3page}"

      # Split landscape A3 page into two portrait A4 pages
      convert "${longa3page}" -crop 50%x100% +repage "${tmpfolder}"/tmppage_a4_%02d.png

      # Reorder
      # Example: 4 A3 pages, printed on both sides
      # yields a brochure with 16 A4 pages
      #
      # 1st A3 page     2nd A3 page     3rd A3 page   ...  8th A3 page
      # |-----,-----|   |-----,-----|   |-----,-----|     |-----,-----|
      # |--16-,---1-|   |---2-,--15-|   |--14-,---3-|     |---8-,---9-|
      # |-----,-----|   |-----,-----|   |-----,-----|     |-----,-----|
      # 
      # results in
      # 1st A4     2nd A4  ... 16th A4 page
      # |-----|    |-----|     |-----| 
      # |---1-|    |---2-|     |--16-|  
      # |-----|    |-----|     |-----|    
      numdoublepage=$(find "${tmpfolder}"/ -type f -name "tmppage_a3*.png" | wc -l)
      numsinglepage=$(( numdoublepage*2 ))
      
      if [[ $((pcnt%2)) -eq 0 ]]; then # double page counter even
        numdest1=$(( pcnt ))
        numdest2=$(( numsinglepage-pcnt+1 ))
      else # double page counter even
        numdest1=$(( numsinglepage-pcnt+1  ))
        numdest2=$(( pcnt ))
      fi

      # Convert to %02d format # TODO: this should be obtained simpler!
      if [[ ${#numdest1} -lt 2 ]] ; then
        numdest1="00${numdest1}"
        numdest1="${numdest1: -2}"
      fi
      if [[ ${#numdest2} -lt 2 ]] ; then
        numdest2="00${numdest2}"
        numdest2="${numdest2: -2}"
      fi

      dest1="page${numdest1}.png"
      dest2="page${numdest2}.png"
      
      mv "${tmpfolder}"/tmppage_a4_00.png "${tmpfolder}/${dest1}"
      mv "${tmpfolder}"/tmppage_a4_01.png "${tmpfolder}/${dest2}"

      let "pcnt+=1"
    done  # loop over pages

    rm "$tmpfolder"/tmppage_*.png
  fi ## split


  # Watermarking
  echo
  echo "Watermarking"

  # Add watermark to image
  for longpage in "${tmpfolder}"/page*.png; do
    page=$(basename "${longpage%.*}")
    echo "Watermark $longpage"
    tmppage="${tmpfolder}"/tmp_$page.png  # save rotated png with alpha channel

    # New Procedure
    # inspired from https://www.imagemagick.org/discourse-server/viewtopic.php?t=33189

    # convert to png32 with alpha channel
    convert $longpage PNG32:$tmppage

    # apply watermark
    convert $tmppage \
      \( -background none -pointsize 100 -fill rgba\(.25,.25,.25,0.05\) label:"${matnum}" -rotate 20 -bordercolor none -border 10 -write mpr:tile +delete \) \
      \( +clone -tile mpr:tile  -draw 'color 0,0 reset' \) \
      -compose over -composite "${tmpfolder}"/watermark_$page.png

    # compress to jpg
    convert -quality "${quality}" "${tmpfolder}"/watermark_$page.png "${tmpfolder}"/rotate_watermark_$page.jpg
  done

  # Convert all images into pdf
  echo
  echo "Convert all images to pdf"
  longoutpdf="${outfolder}"/"${inpdf}"_w.pdf
  img2pdf "${tmpfolder}"/rotate*.jpg --output "${longoutpdf}" --pagesize A4 --author "${lauthor}" --title "${matnum}"

  # remove temporary files
  rm -rf "${tmpfolder}"

}  #done # end of PDF loop

if [[ "${cores}" -gt "1" ]]; then
  echo "Parallel execution with ${cores} cores from now on."
  export -f doit
  parallel -j "${cores}" doit ::: "${infolder}"/*.pdf ::: "${outfolder}" ::: "${dpi}" ::: "${quality}" ::: "${split}" ::: "${author}" ::: "${tmpfolder}"
else
  for longinpdf in "${infolder}"/*.pdf
  do
    doit "${longinpdf}" "${outfolder}" "${dpi}" "${quality}" "${split}" "${author}" "${tmpfolder}"
  done # end of PDF loop
fi
