# exam-scan

Preparing exam scans for ship out: Adding watermarks, encryption and preparing upload to Moodle. 

**Contents**

* `watermark.sh` watermarks each page of PDFs containing exam scans with matriculation number of the respective student
* `encrypt.sh` encrypts PDF with password
* `preparemoodle.sh` prepares for uploading PDFs to moodle via assign module as feedback file for each student
* `batch.sh` calls the three files above successively
* `qr_sort.sh` sorts scanned pages according to the page number, matriculation number and exam and merges them to single PDF files per student and exam.
* `qr_mark.sh` create personalized PDFs from a CSV list of matriculation numbers and a template PDF
* `rename.sh` mass rename exam PDFs based on CSV file to map sequential exam-ids to matriucation numbers (as mandated by Covid-19 rules)

Please note that the four scripts `qr_sort.sh`, `watermark.sh`, `encrypt.sh`, and `preparemoodle.sh` do not depend on each other. 
If you want to use only a subset (or one) of the scripts, you can find their package dependencies further down in the Installation section.

Exemplary outputs can be downloaded:
  * [moodle_feedbacks.zip](https://git.rwth-aachen.de/IENT/exam-scan/-/jobs/artifacts/master/raw/out/moodle_feedbacks.zip?job=test): The zip-Archive to be uploaded to Moodle containing the watermarked and encrypted PDFs for each student.
  * [passwords.csv](https://git.rwth-aachen.de/IENT/exam-scan/-/jobs/artifacts/master/raw/out/passwords.csv?job=test): CSV file containing passwords for each PDF.

 Please note that we also provide a Dockerfile and a pre-built Docker image, see below.

## Quick start

### Prerequisites

* Create PDFs corresponding to each exam
  * Scan the exams and save the scans as PDFs (each page should be A4). For most copy machines, you can save an A3 scan (double page of an exam) as two A4 pages.
  * The filename of each PDF should start with the student's matriculation number (e.g. `123456_Nachname.pdf`).
  * Place all PDFs in a folder, e.g. `pdfs`.
* Create and setup Moodle
  * In your Moodle course room, create an `assign` module following this guideline: https://doc.itc.rwth-aachen.de/x/fwWPAg
  * Download the grading table `Bewertungen.csv` from Moodle via: `Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Bewertungstabelle herunterladen`
* Run `watermark.sh`, `encrypt.sh`, and `preparemoodle.sh` as described in the sections below. In summary, these steps will 
  * watermark each page of each PDF with the corresponding matriculation number,
  * encrypt each PDF with a password (global or per-student) and 
  * construct a zip-archive enabling batch upload and assignment of each PDF to each student in Moodle.
* Upload `moodle_feedbacks.zip` to Moodle
  * `Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Mehrere Feedbackdateien in einer Zip-Datei hochladen`
  * Moodle will check for consistency and prompt errors.


### Install software dependencies

We tested everything only under Ubuntu 18.04 (native or Windows Subsystem for Linux). We also provide a Dockerfile (see Section below).

##### Ubuntu / Debian

```bash
sudo apt-get install bash poppler-utils imagemagick-6.q16 img2pdf parallel qpdf pwgen zip zbar-tools
```

##### Fedora / CentOS

```bash
sudo dnf install bash poppler-utils imagemagick python3-img2pdf parallel qpdf pwgen zip zbar
```

##### macOS

```bash
brew install bash poppler parallel imagemagick qpdf pwgen zip zbar
```

Now everything should be set up.


### Personalized PDFs with QR codes

#### Create PDFs with QR codes

The `qr_mark.sh` script reads in  matriculation numbers from a CSV file and creates a personalized PDF by adding QR code marks to each page.
As base a common template PDF is used.

```bash
./qr_mark.sh --template exam.pdf --csv students.csv --out pdfs/marked
```

### Batch scanning and sorting using QR codes

The `qr_sort.sh` script sorts arbitrarily shuffeled scans of exams based on a QR code marker on each page.
This marker contains the following information:

- Exam ID (random or matriculation number)
- Page number

E.g. `312345-22` for page 22 of an exam with id 312345.

For embedding the QR codes into an existing LaTeX template take a look at our example `example_marker.tex`.
E.g. you can generate the required QR codes by using the `qrcode` package:

```latex
\includepackage{qrcode}
\qrcode[height=2cm, nolinks]{\matrikelnr-\therealpage}
```

We assume that the folder `./pdfs/ingress` holds a set of scans.
The order of pages or grouping of these scans can be arbitrary.
E.g. a single PDF files for all exams is fine or multiple PDF files scanned by different people as well:

- `ingress/steffens_scans.pdf`
- `ingress/christians_scans.pdf`

All pages of these scans will be analyzed for QR code markers.
These markers are then used to sort and group the individual pages for each exam.

Folder `./pdfs/sorted` contains the sorted and merge pages for each exam.
E.g. `./pdfs/sorted/acs-gi4-ss2020-312345.pdf`

```bash
./qr_sort.sh --in ./pdfs/ingress --out ./pdfs/sorted --expected-pages=16
```

When provided, the `--expected-pages` option will check for completeness of each scanned and sorted exam.

### Watermark

We assume that the folder `./pdfs` holds the scans of the exams. 
The filename of each PDF should start with the matriculation number of the student, e.g. `./pdfs/123456_Lastname.pdf`.

```bash
./watermark.sh --in ./pdfs/sorted --out ./pdfs/watermarked --cores 2
```

Folder `./pdfs/watermarked` contains watermarked PDFs, with each page watermarked with the matriculation number of the student.

### Encrypt

Use either a global password by specifying it with the `--password` option or per-student passwords by ommiting `--password`.


```bash
./encrypt.sh --in ./pdfs/watermarked --out ./pdfs/encrypted --password ganzgeheim
```

Folder `./pdfs/encrypted` contains all encrypted PDFs as well as `passwords.csv`, mapping the password of each PDF to the matriculation number.

### Prepare for Moodle batch upload

This step prepares the PDFs for upload to Moodle. First, the grading table `Bewertungen.csv` has to be downloaded from Moodle via:

`Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Bewertungstabelle herunterladen`. 

This step is needed since Moodle does not only need matriculation number, but also last and first name as well as an internal user id, which is stored in `Bewertungen.csv`. 


```bash
./preparemoodle.sh --in ./pdfs/encrypted --csv ./Bewertungen.csv --out ./moodle_feedbacks.zip
```

Then, you can upload `moodle_feedbacks.zip` in Moodle: 
`Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Mehrere Feedbackdateien in einer Zip-Datei hochladen`

Further remarks:
* Exemplary zip archive `moodle_feedbacks.zip` can be downloaded [here](https://git.rwth-aachen.de/IENT/exam-scan/-/jobs/artifacts/master/download?job=test).
* You can also conduct a dry run (neither folders nor zip file are created) via `./preparemoodle.sh --dry [...]`

### Rename exams

During the Covid-19 breakout, personalized exam sheets have been disallowed.
As a consequence, exam sheets must only be generated using sequential or random exam ids.

The `./rename.sh` script can be used to mass-rename exams based on a mapping CSV file:

#### Example `mapping.csv`

```csv
# exam-id,matriculation-number
1,123456
2,252352
3,236422
```

```bash
./rename.sh --csv mapping.csv --in ./pdfs/sorted --out ./renamed --exam systheo
```

#### Adding QR codes to exams based on LaTeX

```latex
\includepackage{qrcode}
\qrcode[height=2cm, nolinks]{\matrikelnr-\therealpage}
```

### Batch job

Or do everything in one step
```bash
./batch.sh --in ./pdfs --csv ./Bewertungen.csv --out ./out --password ganzgeheim --cores 2
```
with folder `out` containing `passwords.csv` and `moodle_feedbacks.zip`.

## Installation

Tested everything under Ubuntu 18.04 (both native and Windows Subsystem for Linux (WSL))

### Dependencies

`watermark.sh`:

* poppler-utils (pdfimages)
* imagemagick-6.q16 (convert)
* img2pdf
* parallel

`encrypt.sh`:

* qpdf
* pwgen

`preparemoodle.sh`:

* zip

Under Ubuntu 18.04:

```bash
sudo apt-get install poppler-utils imagemagick-6.q16 img2pdf parallel qpdf pwgen zip
```

## Docker

Try it out with
```bash
docker run --name examscan --rm -it -v $(pwd):$(pwd) -w $(pwd) \ 
  registry.git.rwth-aachen.de/ient/exam-scan \
  --in in --out out --tmp tmp
```

or build it yourself

```bash
docker build --tag examscan .
docker run --name examscan --rm -it -v $(pwd):$(pwd) -w $(pwd) \ 
  examscan --in in --out out --tmp tmp
```

## Original Authors

Helmut Flasche, Jens Schneider, Christian Rohlfing, IENT RWTH Aachen<br />
Dietmar Kahlen, ITHE RWTH Aachen<br />
Steffen Vogel, Jonathan Klimt, ACS RWTH Aachen

## Who do I talk to?

Servicedesk IT-Center RWTH Aachen <[servicedesk@itc.rwth-aachen.de](mailto:servicedesk@itc.rwth-aachen.de)>
