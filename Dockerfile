# Ubuntu 18.04 (bionic)
# https://hub.docker.com/_/ubuntu/?tab=tags&name=bionic
# OS/ARCH: linux/amd64
ARG ROOT_CONTAINER=ubuntu:bionic-20200403
ARG BASE_CONTAINER=$ROOT_CONTAINER
FROM $BASE_CONTAINER

LABEL maintainer="Christian Rohlfing <rohlfing@ient.rwth-aachen.de>"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
 && apt-get install -yq --no-install-recommends \
    wget \
    poppler-utils \
    imagemagick-6.q16 \
    gsfonts \
    img2pdf \
    parallel \
    qpdf \ 
    pwgen \
    zip \
    zbar-tools \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./*.sh /opt/scripts/

ENTRYPOINT [ "/opt/scripts/batch.sh" ]
