#!/bin/bash
#
# Author: Jens Schneider <schneider@ient.rwth-aachen.de>, Christian Rohlfing <rohlfing@ient.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
infolder="./pdfs/encrypted"
outzip="./moodle_feedbacks.zip"
csv="./Bewertungen.csv"
tmpfolder=$(mktemp -d)
dry=n
nowarn=n

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--csv ] [--out outzip] --  prepares batch upload to Moodle via assignment module. 
PDFs in folder 'in' are moved to folder 'tmp' with a certain folder structure and finally zipped to 'out'. 
Attention: zip-archive 'out' will be overwritten in the following!

Options:
    -h, --help      show this help text
    -i, --in        input folder with PDFs. Default: ${infolder}
    -c, --csv       Moodle grading CSV file, needed to construct the folder names. Default: ${csv}
    -o, --out       output zip archive: ${outzip}
    -d, --dry       flag for dry run, displays only the folder structure inside the output archive
    -t, --tmp       temporary folder Default: ${tmpfolder}
        --nowarn    disables warnings"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:c:o:d,t:v
LONGOPTS=help,in:,csv:,out:,dry,tmp:,nowarn,verbose

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -c|--csv)
            csv="$2"
            shift 2
            ;;
        -o|--out)
            outzip="$2"
            shift 2
            ;;
        -d|--dry)
            dry=y
            shift 1
            ;;
        -t|--tmp)
            tmpfolder="$2"
            shift 2
            ;;
        --nowarn)
            nowarn=y
            shift 1
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$infolder" \
        "$tmpfolder"
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

if ! [ -f "$csv" ]; then
    echo "CSV $csv does not exist. Exiting."
    exit
fi


numlines=$(wc -l "${csv}" | awk '{ print (($1-1)) }')
echo
echo "Preparing for moodle upload"
echo "Processing ${numlines} lines"


if [[ "$dry" = "y" ]]; then
    echo
    echo "Dry run"
    echo
    dryoutput=""
else
    # Remove files
    rm -rf "${tmpfolder}"/*
    rm -rf "${outzip}"
fi

# Loop over all lines in CSV file
numfoundpdfs=0
cnt=0
echo -ne "Start iterating..."
{
    read  # skip first row in CSV file since this should be the header
    while read -r line
    do
        # parse the required fields from the csv file
        id=$(echo $line | awk -F',' '{printf "%s", $1}' | tr -d '"')
        id="${id/"Teilnehmer/in"/}"  # remove "Teilnehmer/in" to get the assignment id
        id="${id/"Participant"/}"  # do the same for "Participant"

         # remove quotation marks from content
        lastname=$(echo $line | awk -F',' '{printf "%s", $2}' | tr -d '"') 
        firstname=$(echo $line | awk -F',' '{printf "%s", $3}' | tr -d '"')
        matnum=$(echo $line | awk -F',' '{printf "%s", $4}' | tr -d '"')

        # if a pdf file for current student exists, create a directory and copy
        # the pdf file to it. The resulting directories can be uploaded to moodle
        longpdffile=$(find ${infolder}/ -type f -name "${matnum}*.pdf")
        
        if [ -f "${longpdffile}" ]; then
            let "numfoundpdfs+=1"
            pdffile=$(basename "${longpdffile}")
            foldername="${lastname},${firstname}_${id}_assignsubmission_file_"

            if ! [[ "$dry" = "y" ]]; then
                #echo "Handle student ${matnum}"
                mkdir -p "${tmpfolder}/${foldername}"
                cp ${longpdffile} "${tmpfolder}/${foldername}/${pdffile}"
            else
                dryoutput="${dryoutput}\n${foldername}/${pdffile}"
            fi
        else
            if [[ "${nowarn}" = "n" ]]; then
                echo "Warning: PDF corresponding to matriculation number ${matnum} (id=${id}, lastname=${lastname}) not available."
            fi
        fi
        
        # Progress
        if ! ((cnt % ((numlines+10)/10))); then
            echo -ne "."
        fi
        let "cnt+=1"
    done 
} < "${csv}"

echo -ne "done."

echo
echo "Found ${numfoundpdfs} PDFs (CSV had ${numlines} entries)"

echo
echo "Searching for matriculation numbers not present in CSV but in PDF input folder:"

# Check for PDFs which are not reflected in CSV (student not registered in Moodle)
numnotfoundmatnums=0
notfoundmatnums=""
for longinpdf in "${infolder}"/*.pdf
do
    # Get matriculation number from file
    inpdf=$(basename "${longinpdf%.*}")  # file name without folder and extension
    matnum=${inpdf:0:6}  # read in first 6 letters

    # Search in CSV
    if ! grep -q ",${matnum}," "${csv}"; then
        let "numnotfoundmatnums+=1"
        notfoundmatnums="${notfoundmatnums}${matnum},"
        if [[ "${nowarn}" = "n" ]]; then
            echo "Warning: Could not find ${matnum} in CSV."
        fi
    fi
done

if [ "$numnotfoundmatnums" -gt "0" ]; then
    echo "I could not find the following ${numnotfoundmatnums} matriculation numbers in CSV:"
    echo "${notfoundmatnums}"
fi


# Zipping
if ! [[ "$dry" = "y" ]]; then
    DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    echo
    echo "Zipping"

    cd "${tmpfolder}"
    zip -r "${DIR}/${outzip}" ./ -x ".gitkeep"
    cd "${DIR}"
else
    echo -e "\n\nResults from dry run:\n${dryoutput}"
fi
