#!/bin/bash
#
# Author: Helmut Flasche <flasche@ient.rwth-aachen.de>, Christian Rohlfing <rohlfing@ient.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
v=n
qr=n
password=-
infolder="./pdfs"
csv="./Bewertungen.csv"
outfolder="./out"
cores=$(nproc)
dpi=250
quality=25
tmpfolder="$(mktemp -d)"

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [-out outfolder] --  watermark and encrypts exams and prepares everything for moodle upload. 
Attention: contents of folder 'out' will be deleted in the beginning!

Options:
    -h, --help      show this help text
    -i, --in        input folder with PDFs. Default: ${infolder}
    -c, --csv       Moodle grading CSV file, needed to construct the folder names for moodle zip. Default: ${csv}
    -o, --out       output folder containing passwords.csv and moodle_feedbacks.zip. Default: ${outfolder}
    -p, --password  sets global password. Default: empty, such that each PDF gets a custom password generated with 'pwgen'
    -j, --cores     number of cores for watermarking. Default: ${cores}
    -d, --dpi       dpi parameter for conversion from pdf to images. Default: ${dpi}
    -q, --quality   quality parameter for jpeg. Default: ${quality}
    -t, --tmp       temporary folder"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:c:o:p:d:q:t:vj:
LONGOPTS=help,in:,csv:,out:,password:,cores:,dpi:,quality:,tmp:,verbose

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -c|--csv)
            csv="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -p|--password)
            password="$2"
            shift 2
            ;;
        -j|--cores)
            cores="$2"
            shift 2
            ;;
        -d|--dpi)
            dpi="$2"
            shift 2
            ;;            
        -q|--quality)
            quality="$2"
            shift 2
            ;;            
        -t|--tmp)
            tmpfolder="$2"
            shift 2
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder" \
        "$tmpfolder" 
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

# Delete old files
rm -rf "${tmpfolder}"/*
rm -rf "${outfolder}"/*

outfolder_watermarked="${tmpfolder}/out_watermarked"
outfolder_encrypted="${tmpfolder}/out_encrypted"
tmpfolder2="${tmpfolder}/tmp"

mkdir -p "${outfolder_watermarked}"
mkdir -p "${outfolder_encrypted}"
mkdir -p "${tmpfolder2}"


# Watermark
./watermark.sh --in "${infolder}" --out "${outfolder_watermarked}" --cores "${cores}"

# Encrypt
./encrypt.sh --in "${outfolder_watermarked}" --out "${outfolder_encrypted}" --password "${password}"
cp "${outfolder_encrypted}/passwords.csv" "${outfolder}/passwords.csv"

# Prepare moodle
./preparemoodle.sh --in "${outfolder_encrypted}" --out "${outfolder}/moodle_feedbacks.zip" --csv $csv --tmp "${tmpfolder2}"
